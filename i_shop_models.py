class Discount(models.Model):
    """
    """
    from = models.DateTimeField()
    to = models.DateTimeField()
    percent = models.IntegerField(default=0, validators=[
                                                    MaxValueValidator(100),
                                                    MinValueValidator(0)
                                                ])

    def __str__(self):
        return self.percent


class AbstractObjectWithDiscount(models.Model):
    """
    Слово "абстрактный" в этой модели упоминается 3 раза.
    """
    name = models.CharField(max_length=200)
    discount = models.ForeignKey(Discount, null=True)

    class Meta:
        abstract = True


class Client(AbstractObjectWithDiscount):
    """
    Обычно я серьезно отношусь к комментариям. Но это тест..
    """
    is_great_man = models.BooleanField(default=True, verbose_name='Y')

    class Meta:
        verbose_name = 'Чувак'
        verbose_name_plural = 'Чуваки'


class Brand(AbstractObjectWithDiscount):
    """
    -*-*-
    """
    smth = models.SomeCoolField()


class Group(AbstractObjectWithDiscount):
    """
    """


class ProductManager(models.Manager):
    """
    Не пригодился в итоге.
    """
    pass


class Product(AbstractObjectWithDiscount):
    """
    Big and usefull comment about product model.
    """
    brand = models.ForeignKey(Brand, verbose_name='B', null=True)
    group = models.ForeignKey(Group, verbose_name='G', null=True)
    price = models.PositiveIntegerField(default=0)
    manager = ProductManager()

    def get_discount(self, client_percent=0):
        return max(self.discount.percent,
                   self.brand.discount.percent,
                   self.group.discount.percent,
                   client_percent)

    @property
    def actual_price(self):
        return self.price * (self.get_discount() / 100)

    def get_client_price(self, client_discount_percent):
        return self.price * (self.get_discount(client_discount_percent) /100)
