@require_http_methods(['GET'])
def product_list_view(request):
    if request.GET.get('from_1000_to_10000'):
        # Генераторам быть!
        filtered = (x for x in Product.objects.all if x.actual_price < 1000 \
                    and x.actual_price < 10000)
    else:
        filtered = Product.objects.all()

    product_list = sorted(filtered, key=lambda t: t.actual_price)
    return render(request, 'product_list.html', {'product_list': product_list})


@require_http_methods(['GET'])
def buy_product(request, slug):
    """
    Вью страницы, где клиент уже покупает товар.
    """
    product = get_object_or_404(Product, slug=slug)
    price = product.get_client_price(client.discount.percent)
    return render(request, 'product_details.html', {'product': product,
                                                    'price': price,})
