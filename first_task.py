# coding:utf-8
"""
    Python 2.7
"""
import re

input_string = 'asdfd((asdf)(asdf'

def first_task_wo_re(string):
    while string[len(string)-1] != ')':
        # Все ломается, если строка начинается с незакрытой скобки. костыльнем.
        if len(string) == 1:
            string = ''
            break
        string=string[:-1]
    return string

def first_task_w_re(string):
    string = re.sub(r'\([^)]*$', '', string)
    return string


if __name__=='__main__':
    print first_task_wo_re(input_string)
    print first_task_w_re(input_string)
