import unittest

from first_task import first_task_wo_re, first_task_w_re

class FirstTaskTest(unittest.TestCase):
    def setUp(self):
        self.fi_string = 'asdfd((asdf)(asdf'
        self.s_string = '((asdas123)()'
        self.t_string = 'qwe(asd))'
        self.f_string = '('

    def test_returns_string(self):
        self.assertEqual(type(first_task_w_re(self.fi_string)), str)
        self.assertEqual(type(first_task_wo_re(self.fi_string)), str)

    def test_w_re_returns_right_string(self):
        self.assertEqual(first_task_w_re(self.fi_string), 'asdfd((asdf)')
        self.assertEqual(first_task_w_re(self.s_string), '((asdas123)()')
        self.assertEqual(first_task_w_re(self.t_string), 'qwe(asd))')
        self.assertEqual(first_task_w_re(self.f_string), '')

    def test_wo_re_returns_right_string(self):
        self.assertEqual(first_task_wo_re(self.fi_string), 'asdfd((asdf)')
        self.assertEqual(first_task_wo_re(self.s_string), '((asdas123)()')
        self.assertEqual(first_task_wo_re(self.t_string), 'qwe(asd))')
        self.assertEqual(first_task_wo_re(self.f_string), '')


if __name__=='__main__':
    unittest.main()
